
#include <CircularBuffer.h>
#include <LiquidCrystal.h>
//LCD pin to Arduino
const int pin_RS = 8; 
const int pin_EN = 9; 
const int pin_d4 = 4; 
const int pin_d5 = 5; 
const int pin_d6 = 6; 
const int pin_d7 = 7; 
const int pin_BL = 10; 
LiquidCrystal lcd( pin_RS,  pin_EN,  pin_d4,  pin_d5,  pin_d6,  pin_d7);

String incoming ="";
String incomString;
String readString;

void setup() {
  analogWrite(pin_BL,0);
  lcd.begin(16, 2);
  Serial.begin(9600);
  Serial.flush();
}

void loop() {

  while (Serial.available()) {
    delay(2);  //delay to allow byte to arrive in input buffer
    char c = Serial.read();
    readString += c;
  }
  delay(5);

  if (readString.length() >0) {
    analogWrite(pin_BL,150);
    delay(5);
 // Serial.print(readString);
 // Serial.print(readString.length());
    incomString=readString;
    delay(5);
    readString="";
   }


  if(incomString.length() >0){
    lcdPrint(incomString);
    incomString="";
    }
  }
    
  void lcdPrint(String incoming){
    CircularBuffer<char,16> charBuff;   
    String toPrint;
    lcd.clear();
    
    for (int i =0; i< incoming.length(); i++){
      charBuff.push(incoming.charAt(i));
      
      for(int x=0; x<charBuff.size();x++){
        toPrint+=charBuff[x]; 
       }
      
      lcd.clear();
      lcd.setCursor(0,1);
      lcd.print(toPrint);
      lcd.setCursor(0,0);
      lcd.print("Latest News:");
      delay(200);
      toPrint="";
      }
      
    delay(1000);
    lcd.clear();
    analogWrite(pin_BL,0);
  }
